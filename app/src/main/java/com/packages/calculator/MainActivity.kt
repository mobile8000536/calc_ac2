package com.packages.calculator

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.packages.calculator.backend.calcular
import com.packages.calculator.backend.validarFormula
import com.packages.calculator.ui.theme.BackC
import com.packages.calculator.ui.theme.CalculatorTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalculatorTheme {
                Calculadora()
            }
        }
    }
}


@Composable
fun Calculadora (){

    var visor by remember { mutableStateOf("") } // visor principal da calculadora
    val buttonParams = listOf(15,40,12) // parametros dos botoes: TextSize / padding horizontal /padding vertical

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(Color.Gray)
            .fillMaxSize()
    ) {
        Spacer(modifier = Modifier.height(30.dp))
        Row (
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .padding(horizontal = 30.dp)
        ){
            Text(text = visor,                 // visor principal
                fontSize = 30.sp,
                lineHeight = 16.sp,
                fontWeight = FontWeight.Bold,
                maxLines = 1,
                softWrap = false,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(45.dp)
                    .background(Color.White, CircleShape)
                    .border(1.dp, Color.Black, CircleShape)
                    .padding(horizontal = 20.dp)
                )
        }

        Spacer(modifier = Modifier.height(50.dp))


        // Bloco dos botões
        Row (
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp, vertical = 5.dp),
            horizontalArrangement = Arrangement.SpaceEvenly) {
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor = visor.plus(" ( ")
                          },
                content = {Text(text = "(",fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                    visor = ""
                    }
                    visor += " ) "},
                content = {Text(text = ")", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    if(visor != "") {visor = visor.trim().dropLast(1).trim()}
                          },
                content = {Text(text = "C", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {visor = ""},
                content = {Text(text = "AC", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(colorResource(id = R.color.ACbutton)),
                border = BorderStroke(1.dp,Color.LightGray)
            )
        }
        Row (
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp, vertical = 15.dp),
            horizontalArrangement = Arrangement.SpaceEvenly) {
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "7"},
                content = {Text(text = "7", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "8"},
                content = {Text(text = "8", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "9"},
                content = {Text(text = "9", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += " / "},
                content = {Text(text = "/", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
        }
        Row (
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp, vertical = 15.dp),
            horizontalArrangement = Arrangement.SpaceEvenly) {
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "4"},
                content = {Text(text = "4", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "5"},
                content = {Text(text = "5", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "6"},
                content = {Text(text = "6", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += " * "},
                content = {Text(text = "*", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
        }
        Row (
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp, vertical = 15.dp),
            horizontalArrangement = Arrangement.SpaceEvenly) {
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "1"},
                content = {Text(text = "1", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "2"},
                content = {Text(text = "2", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "3"},
                content = {Text(text = "3", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += " - "},
                content = {Text(text = "-", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
        }
        Row (
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp, vertical = 15.dp),
            horizontalArrangement = Arrangement.SpaceEvenly) {
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += "0"},
                content = {Text(text = "0", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += ","},
                content = {Text(text = ",", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    if ('!' in visor){
                        visor = ""
                    }
                    visor += " + "},
                content = {Text(text = "+", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(BackC),
                border = BorderStroke(1.dp,Color.LightGray)
            )
            Button(
                onClick = {
                    val (isValid,formula) = validarFormula(visor)
                    visor = if (isValid){
                        val a = calcular(formula!!)
                        a
                    }else{
                        "!!Erro!! "
                    }
                          },
                content = {Text(text = "=", fontSize = buttonParams[0].sp)},
                contentPadding = PaddingValues(horizontal = buttonParams[1].dp, vertical = buttonParams[2].dp),
                colors = ButtonDefaults.buttonColors(colorResource(id = R.color.EqualsButton)),
                border = BorderStroke(1.dp,Color.LightGray)
            )
        }
    }
}