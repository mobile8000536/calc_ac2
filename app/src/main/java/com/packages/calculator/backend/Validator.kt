package com.packages.calculator.backend

fun validaVirgula(equacao: String): Pair<Boolean,MutableList<String>?>{

    // Converte para lista removendo espaços e indices vazios->
    val eList = equacao.split(" ").toMutableList()
    eList.removeAll{it == ""}

    //teste para multiplas virgulas
    for (item in eList) {
        val vNum = item.count { it == ',' }
        if ( vNum > 1 ) {
            return Pair(false,null)
        }else if(vNum == 1){
            val ePonto = item.replace(",",".")
            eList[eList.indexOf(item)] = ePonto
        }
    }
    return Pair(true, eList)
}


fun validaEquacao(equacao: String): Boolean {
    val pilha = mutableListOf<Char>()

    for (char in equacao) {
        if (char == '(') {
            pilha.add(char)
        } else if (char == ')') {
            if (pilha.isEmpty() || pilha.last() != '(') {
                return false
            }
            pilha.removeAt(pilha.size - 1)
        }
    }

    return pilha.isEmpty()
}

//função que chama os testes
fun validarFormula(equacao: String): Pair<Boolean, MutableList<String>?> {
    val a = validaEquacao(equacao)
    val (b,lista) = validaVirgula(equacao)
    return if (a && b){
        Pair(true,lista)
    }
    else{
        Pair(false,null)
    }
}