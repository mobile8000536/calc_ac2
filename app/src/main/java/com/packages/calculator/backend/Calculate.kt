package com.packages.calculator.backend

//função para resolver soma e subtração
fun sumSub(formula: MutableList<String>): String {
    while (formula.size != 1) {
        val num1 = formula[0].toDouble()
        val num2 = formula[2].toDouble()
        val result = if (formula[1] == "+") num1 + num2 else num1 - num2

        for (i in 0 until 3) {
            formula.removeAt(0)
        }
        formula.add(0, result.toString())
    }
    return formula[0]
}

//função para resolver Divisão e Multiplicação
fun divMult(formula: MutableList<String>): MutableList<String>? {
    var index = 0

    while (index < formula.size) {
        when (formula[index]) {
            "*" -> {
                val num1 = formula[index - 1].toDouble()
                val num2 = formula[index + 1].toDouble()
                val result = num1 * num2

                formula[index - 1] = result.toString()
                formula.removeAt(index)
                formula.removeAt(index) // Remove o operador
            }
            "/" -> {
                val num1 = formula[index - 1].toDouble()
                val num2 = formula[index + 1].toDouble()
                if (num2 == 0.0){            //teste para caso de divisão por 0
                    return null
                }
                val result = num1 / num2

                formula[index - 1] = result.toString()
                formula.removeAt(index)
                formula.removeAt(index) // Remove o operador
            }
            else -> index++ // Incrementa apenas quando não é operador
        }
    }
    return formula
}

//função que isola e calcula equações dentro de parenteses
fun eliminaParenteses(visor:MutableList<String>): MutableList<String>{
    while ("(" in visor){
        val inicio = visor.indexOf("(")
        val fim = visor.indexOf(")")
        val temp = visor.slice(inicio+1 until fim).toMutableList()
        for(i in 0.. fim - inicio){
            visor.removeAt(inicio)
        }
        visor.add(inicio,sumSub(divMult(temp)!!))
    }
    return visor
}

//função que realiza o calculo seguindo ordem de precedencia  *parenteses -> div/mul -> soma/sub*
fun calcular(visor: MutableList<String>): String {
    val rEP = eliminaParenteses(visor)
    val rDM = divMult(rEP)
    if (rDM != null){
        return String.format("%.6f", sumSub(rDM).toDouble()).replace(".",",") //conversão de ponto para vírgula e formatação para 6 casas decimais
    }
    return "!!ERRO!! - divisao por 0"
}